import React from 'react'
import { Image, StyleSheet, Text, View, Button, TouchableOpacity } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import 'react-native-gesture-handler';
import Login from './Login';
import Register from './Register';


export default function Screen({navigation}) {
  return (
    <View style={styles.container}>
      <View>
      <Image
              source={require('./assets/screen.png')}
            />
      </View>
      <View style={styles.footer}>
        <TouchableOpacity style={{height: 32, width: 300, backgroundColor: '#4FF2EB', borderRadius: 10}}
          onPress={()=>navigation.navigate('Register')}>
          <Text style={{
                    color: 'black',
                    fontSize: 20,
                    textAlign: 'center',
                  }}>Register</Text>
          </TouchableOpacity>
        <TouchableOpacity style={{height: 32, width: 300, backgroundColor: 'white', borderWidth:1, borderColor:'black', borderRadius: 10}}
          onPress={()=>navigation.navigate('Login')}>
          <Text style={{
                    color: '#4FF2E8',
                    fontSize: 20,
                    textAlign: 'center',
                  }}>Login</Text>
          </TouchableOpacity>
      </View>
    </View>
    )
}

const styles = StyleSheet.create ({
  container:{
    flex: 1,
    backgroundColor: '#B9FFF2',
    justifyContent: 'flex-start'
  },
  header:{
    height: 50,
    backgroundColor: '#517DA2',
    marginTop: 30,
    justifyContent: 'center'
  },
  subHeader:{
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 18,
    justifyContent: 'space-between'
  },
  subContentHeader:{
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  footer: {
    flex: 3,
    flexDirection: 'column',
    alignItems: 'center',
    paddingVertical: 10,
    justifyContent: 'space-around',
    borderRadius: 25,
    backgroundColor: 'white',
  }
})