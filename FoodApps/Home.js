import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View, Image, Button, SafeAreaView, FlatList, TouchableOpacity } from 'react-native'
import { Data }from './data'
import { MaterialIcons } from '@expo/vector-icons';

export default function Home({route, navigation}) {
  
  const categories = ['Makanan', 'Minuman'];
  const [categoryIndex, setCategoryIndex] = useState(0);
  const Card = ({Data})

  return (
    <SafeAreaView style={{flex:1, paddingHorizontal:20, backgroundColor:'white'}}>
      <View style={styles.header}>
        <Text style={{fontSize: 25, fontWeight: 'bold'}}>Selamat Datang</Text>
        <Text style={{fontSize: 35, fontWeight: 'bold', color: '#4FF2EB'}}>Resep Masakan</Text>
      </View>
      <View style={styles.categoryContainer}>
        {categories.map((item, index) => (
          <TouchableOpacity key={index} activeOpacity={0.8} onPress={()=>setCategoryIndex(index)}>
            <Text style={[styles.categoryText, categoryIndex == index && styles.categoryTextSelected]}>{item}</Text>
          </TouchableOpacity>
        ))}
      </View>
      <FlatList 
        columnWrapperStyle={{justifyContent:'space-between'}}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{marginTop:10, paddingBottom:30,}}
        numColumns={2} 
        data={Data} 
        keyExtractor={(item)=>item.id}
        renderItem={({item})=> {

        return(
          <View style={styles.card}>
            <View style={{alignItems:'flex-end'}}>
              <View style={{width:30, height:30, borderRadius:15, alignItems:'center', justifyContent:'center', backgroundColor: 'white'}}>
                <MaterialIcons name="favorite" size={24} color="black" />
              </View>
            </View>
            <View style={{height:100, alignItems:'center'}}>
              <Image style={{flex:1, width:120, marginTop:2}} source={item.image} />
            </View>
            <Text style={{fontWeight:'bold', fontSize:17, marginTop:10, alignSelf:'center'}}>{item.title}</Text>
            <TouchableOpacity style={{height: 25, width: 100, backgroundColor: 'white', borderRadius: 10, alignSelf:'center'}} 
              onPress={()=>navigation.navigate('Desc', item)}>
              <Text style={{color: 'black',fontSize: 16,textAlign: 'center'}}>Resep</Text>
            </TouchableOpacity>
          </View>
        )}}
        />
    </SafeAreaView>
  )
}
const styles = StyleSheet.create({
  header:{
    marginTop:20,
    flexDirection: 'column',
    justifyContent: 'space-between'
  },  
  categoryContainer:{
    flexDirection: 'row',
    marginTop: 30,
    marginBottom: 20,
    justifyContent: 'space-around'
  },
  categoryText:{
    fontSize: 16,
    color:'grey',
    fontWeight: 'bold'
  },
  categoryTextSelected:{
    color: '#4FF2EB',
    paddingBottom:5,
    borderBottomWidth:2,
    borderColor: '#B9FFF2'
  },
  card:{
    height:225,
    width:155,
    backgroundColor: '#B9FFF2',
    marginHorizontal:2,
    borderRadius:10,
    marginBottom:20,
    padding:8
  }
})