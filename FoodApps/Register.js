import React, { useState, useEffect } from "react";
import { initializeApp } from "firebase/app";
import { Image, StyleSheet, Text, View, TextInput, Button, SafeAreaView, TouchableOpacity, ScrollView } from "react-native";
import {StackNavigator} from '@react-navigation/stack';
import {Ionicons} from '@expo/vector-icons';
import { Entypo } from '@expo/vector-icons';
import * as firebase from 'firebase'

export default function Register({ navigation }) {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [password, setPassword] = useState("");
  

  const firebaseConfig = {
  apiKey: "AIzaSyAUEgUEK7gQhNa1I6rcNxMd_Qd7Isu7jQY",
  authDomain: "authenticationfirebasern-cee29.firebaseapp.com",
  databaseURL: "https://authenticationfirebasern-cee29-default-rtdb.firebaseio.com",
  projectId: "authenticationfirebasern-cee29",
  storageBucket: "authenticationfirebasern-cee29.appspot.com",
  messagingSenderId: "748656532211",
  appId: "1:748656532211:web:7d945b308967d91d86b586"
};
// Initialize Firebase
  if(!firebase.apps.length){
    firebase.initializeApp(firebaseConfig);
  }

  const submit=()=>{
    const data = {
      username, email, phone, password
    }
    console.log(data)
    firebase.auth().createUserWithEmailAndPassword(email, password)
    .then(()=>{
      console.log("Register Sukses")
      navigation.navigate("MyTabs", username);
    }).catch((err)=>{
      console.log(err)
    })
  }
  	return(
    <ScrollView>
    <View style={styles.container}>
      <Image 
        source={require('./assets/Top.png')}
        />
        <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal:30}}>
          <View>
            <TouchableOpacity onPress={()=>navigation.goBack()}>
            <Ionicons name="ios-arrow-back" size={24} color="black" />
          </TouchableOpacity>
        </View>
        <Entypo name="dots-three-vertical" size={24} color="black" />
        </View>      
      <Image 
        source={require('./assets/screen1.png')}
        />
      <View style={styles.content}>
        <Text style={styles.titleName}>Sign Up</Text>
        <View style={{flexDirection: 'column'}}>
          <TextInput
          style={styles.input}
          placeholder="Username"
          value={username}
          onChangeText={(value)=>setUsername(value)}
        />
        <TextInput
          style={styles.input}
          placeholder="Email"
          value={email}
          onChangeText={(value)=>setEmail(value)}
        />
        <TextInput
          style={styles.input}
          placeholder="Number Phone"
          value={phone}
          onChangeText={(value)=>setPhone(value)}
        />
        <TextInput
          style={styles.input}
          placeholder="password"
          value={password}
          onChangeText={(value)=>setPassword(value)}
        />
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
          <View>
            <TouchableOpacity onPress={()=>navigation.navigate('Login')}>
            <Text style={{color: '#4FF2E8', textAlign: 'center'}}>Already have an account?</Text>
          </TouchableOpacity>
        </View>
        <View style={{paddingLeft: 120}}>
          <TouchableOpacity onPress={submit}>
          <Text style={{color: 'black'}}>Sign Up</Text>
          </TouchableOpacity>
        </View>
        </View>
      </View>
    </View>
    </ScrollView>
  );
}

const styles=StyleSheet.create({
  container:{
    flex: 1,
    backgroundColor: '#B9FFF2',
    
  },
  content: {
    flex: 3,
    alignItems: 'center',
    paddingBottom: 20,
    justifyContent: 'space-around',
    borderRadius: 25,
    backgroundColor: 'white',
  },
  titleName: {
    paddingLeft: 30,
    textAlign: 'left',
    alignSelf: 'flex-start',
    color: 'black',
    fontSize: 30,
    fontWeight: 'bold'
  },
  input: {
    borderBottomWidth: 1,
    paddingVertical: 10,
    borderRadius: 5,
    width: 300,
    marginBottom: 10,
    paddingHorizontal: 10,
  },
});