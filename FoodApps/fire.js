import firebase from 'firebase'

const firebaseConfig = {
  apiKey: "AIzaSyAUEgUEK7gQhNa1I6rcNxMd_Qd7Isu7jQY",
  authDomain: "authenticationfirebasern-cee29.firebaseapp.com",
  databaseURL: "https://authenticationfirebasern-cee29-default-rtdb.firebaseio.com",
  projectId: "authenticationfirebasern-cee29",
  storageBucket: "authenticationfirebasern-cee29.appspot.com",
  messagingSenderId: "748656532211",
  appId: "1:748656532211:web:7d945b308967d91d86b586"
};

const fire = firebase.initializeApp(firebaseConfig);