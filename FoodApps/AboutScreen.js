import React, { useEffect } from 'react'
import { useState } from 'react'
import { StyleSheet, Text, View, Image, Button, SafeAreaView, FlatList, TouchableOpacity } from 'react-native'
import {Data1} from './data1'
import { FontAwesome } from '@expo/vector-icons';

export default function AboutScreen({route, navigation}) {
  const username = route.params;
  const Card = ({Data1})
  return (
    <SafeAreaView style={{flex:1, paddingHorizontal:20, backgroundColor:'white'}}>
      <View style={styles.header}>
        <Text style={{fontSize: 25, fontWeight: 'bold', marginBottom: 20}}>Tentang Saya</Text>
        <FontAwesome name="user-circle-o" size={200} color="grey" />
      </View>
      <View style={{backgroundColor: '#B9FFF2', alignSelf: 'center', borderRadius:20, height:40, width:200, justifyContent:'space-around',paddingBottom:5, marginTop:20}}>
        <Text style={{alignSelf:'center', fontSize:20, fontWeight:'bold'}}>Resep Saya</Text>
      </View>
      <View style={styles.userContainer}>
          <FlatList 
        columnWrapperStyle={{justifyContent:'space-around'}}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{marginTop:10, paddingBottom:30,}}
        numColumns={2} 
        data={Data1} 
        keyExtractor={(item)=>item.id}
        renderItem={({item})=> {

        return(
          <View style={styles.card}>
            <View style={{height:100, alignItems:'center'}}>
              <Image style={{flex:1, width:120, marginTop:2}} source={item.image} />
            </View>
            <Text style={{fontWeight:'bold', fontSize:17, marginTop:10, alignSelf:'center'}}>{item.title}</Text>
            <TouchableOpacity style={{height: 25, width: 100, backgroundColor: 'white', borderRadius: 10, alignSelf:'center'}} 
              onPress={()=>navigation.navigate('Desc', item)}>
              <Text style={{color: 'black',fontSize: 16,textAlign: 'center'}}>Resep</Text>
            </TouchableOpacity>
          </View>
        )}}
        />
        </View>
      
      
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  header:{
    marginTop:30,
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center'
  },  
  userContainer:{
    flex:0.94,
    backgroundColor:'#E0DFDF',
    margin:7,
    borderRadius:20,
    marginTop:30,
    justifyContent: 'space-around',
  },
  userContent:{
    backgroundColor: '#FFFFFF',
    justifyContent: 'space-around',
    alignSelf: 'center',
    borderRadius:20,
    height:60,
    width:200
  },
  userContent1:{
    backgroundColor: '#FFFFFF',
    justifyContent: 'space-around',
    alignSelf: 'center',
    borderRadius:20,
    height:250,
    width:250
  },
  card:{
    height:190,
    width:140,
    backgroundColor: '#B9FFF2',
    marginHorizontal:2,
    borderRadius:10,
    marginBottom:20,
    padding:8
  },
  profile:{
    fontSize: 16,
    padding: 10
  }
})
