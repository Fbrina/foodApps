import React, { useState, useEffect } from "react";
import { Image, StyleSheet, Text, View, TextInput, Button, SafeAreaView, TouchableOpacity, ScrollView } from "react-native";
import fire from './fire';
import * as firebase from 'firebase'
import {StackNavigator} from '@react-navigation/stack';
import {Ionicons} from '@expo/vector-icons';
import { Entypo } from '@expo/vector-icons';

export default function Login({navigation}){
  
  const [email, setEmail] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [emailError, setEmailError] = useState("");
  const [passwordError, setPasswordError] = useState("");
  const [hasAccount, setHasAccount] = useState(false);

  const clearInputs = () =>{
    setEmail('');
    setPassword('');
  }

  const clearErrors = () =>{
    setEmailError('');
    setPasswordError('');
  }

  const handleLogin = () =>{
    clearErrors();
    fire
      .auth()
      .signInWithEmailAndPassword(email, password)
      .catch((err)=>{
        console.log(err)
        }
      );
  };

  const handleSignUp = () =>{
    clearErrors();
    fire
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .catch((err)=>{
        console.log(err)
        }
      );
  }

  const authListener = () =>{
    firebase.auth().onAuthStateChanged((username)=>{
      if(username){
        clearInputs();
        setUsername(username);
      }else{
        setUsername("");
      }
    })
  }

  useEffect(() => {
    authListener();
  }, []);
  return(
    <ScrollView>
    <View style={styles.container}>
      <Image 
        source={require('./assets/Top.png')}
        />
        <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal:30}}>
          <View>
            <TouchableOpacity onPress={()=>navigation.goBack()}>
            <Ionicons name="ios-arrow-back" size={24} color="black" />
          </TouchableOpacity>
        </View>
        <Entypo name="dots-three-vertical" size={24} color="black" />
        </View>      
      <Image 
        source={require('./assets/screen2.png')}
        />
      <View style={styles.content}>
        <Text style={styles.titleName}>Sign In</Text>
        <View style={{flexDirection: 'column'}}>
        <TextInput
          style={styles.input}
          placeholder="Username"
          value={username}
          onChangeText={(value)=>setUsername(value)}
        />
        <TextInput
          style={styles.input}
          placeholder="Email"
          value={email}
          onChangeText={(value)=>setEmail(value)}
        />
        <TextInput
          style={styles.input}
          placeholder="password"
          value={password}
          onChangeText={(value)=>setPassword(value)}
        />
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
          <View>
            <TouchableOpacity onPress={()=>navigation.navigate('Login')}>
            <Text style={{color: '#4FF2E8', textAlign: 'center'}}>Already have an account?</Text>
          </TouchableOpacity>
        </View>
        <View style={{paddingLeft: 120}}>
          <TouchableOpacity onPress={()=>navigation.navigate("MyTabs", username)}>
          <Text style={{color: 'black'}}>Sign In</Text>
          </TouchableOpacity>
        </View>
        </View>
      </View>
      
    </View>
    </ScrollView>
  );
}

const styles=StyleSheet.create({
  container:{
    flex: 1,
    backgroundColor: '#B9FFF2',
    
  },
  header: {
    flexDirection: 'row',
    paddingHorizontal:20,
    justifyContent: 'space-between',
    alignItems: 'flex-start'
  },
  content: {
    flex: 3,
    alignItems: 'center',
    paddingBottom: 20,
    justifyContent: 'space-around',
    borderRadius: 25,
    backgroundColor: 'white',
  },
  titleName: {
    paddingLeft: 30,
    textAlign: 'left',
    alignSelf: 'flex-start',
    color: 'black',
    fontSize: 30,
    fontWeight: 'bold'
  },
  input: {
    borderBottomWidth: 1,
    paddingVertical: 10,
    borderRadius: 5,
    width: 300,
    marginBottom: 10,
    paddingHorizontal: 10,
  },
});