const Data1 =[
    {
        id : "1",
        harga: "9000000",
        image: require("./assets/donat.jpg"),
        title : "Donat Kentang",
        desc : "Resep Donat Kentang",
        desc1: "• Tepung terigu ½ kg • Kentang yang sudah dikukus dan dihaluskan sebanyak 200 gram • Gula pasir 4 sdm • Mentega 3 sdm • Ragi instan 11 g • Air 200 cc • susu bubuk 3 sdm • Susu kental manis 3 sdm • Garam 2 sdt • Telur 2 butir",
        desc2: "1.) Kocok 2 butir telur dalam mangkuk hingga berbusa, tambahkan garam, gula pasir, tepung terigu, dan mentega. Aduk hingga rata.                                            2.) Tuang kentang yang sudah dihaluskan                      3.) Masukkan ragi ke dalam air dan aduk hingga larut, lalu masukkan air ragi tersebut dalam adonan.              4.) Secara perlahan uleni adonan hingga kalis.                                              5.) Tutup mangkok dengan kain. Diamkan adonan selama 1 jam agar mengembang.                                                          6.) Setelah 1 jam berlalu, Anda bisa mulai memanaskan minyak goreng.                                                                7.) Mulailah membentuk adonan menjadi bentuk donat, jangan lupa untuk memberi lubang di tengah.               8.) Masukkan adonan yang telah dibentuk tadi ke dalam penggorengan. Agar tidak mudah gosong, gunakan api kecil. Tunggu hingga adonan donat telah berubah warna menjadi kecokelatan. Lalu angkat dan biarkan dingin. 9.) Setelah donat dingin, olesi donat dengan mentega. 10.) Anda bisa memberikan toping donat dengan meses, keju, gula salju, atau apapun yang Anda inginkan.",
        type :"komputer Kantor"
    },
    {
        id : "2",
        image: require("./assets/kentang.jpg"),
        title : "Kentang Goreng",
        desc : "Resep Kentang Goreng",
        desc1 : "• Kentang ukuran besar • Es batu • Kaldu sapi • Susu kedelai • Corn syrup • Minyak untuk menggoreng ",
        desc2 : "1.) Langkah pertama, siapkan es batu, kaldu sapi, susu kedelai, corn syrup dan soda kue lalu aduk semua bahan tersebut dalam satu mangkuk. Kemudian sisihkan terlebih dahulu dan siapkan kentang goreng.                                                       2.) Cari kentang yang ukurannya besar agar ukuran kentang goreng panjang. Setelah itu kupas kentang, cuci bersih lalu potong memanjang.                                                         3.) Setelah kentang dicuci bersih, masukkan ke dalam adonan kaldu yang sudah dibuat. Setelah itu letakkan mangkuk berisi kentang dan kaldu ke kulkas dan diamkan selama 5 menit.                                                                              4.) Selanjutnya masuk pada langkah menggoreng kentang. Pada tahap ini, kamu harus menyiapkan minyak secukupnya untuk menggoreng lalu panaskan minyak di atas wajan.                                                                5.) Pastikan minyaknya sudah panas hingga 200 derajat celsius. Lalu goreng kentang tersebut selama 3 menit dalam minyak panas.                                                                6.) Setelah itu tiriskan kentang dan beri garam agar terasa lebih gurih. Akhirnya french fries ala McDonald's siap disajikan.",
        type :"komputer Kantor"
    },
    {
        id : "3",
        image: require("./assets/baksoMercon.jpg"),
        title : "Bakso Mercon",
        desc : "Resep Bakso Mercon",
        desc1 : "• Bakso sapi 500 gram • Daun salam 2 lembar • Daun jeruk 3 lembar • Kecap manis 2 sdm • Air 100 ml • Garam secukupnya • Gula secukupnya                          => BUMBU HALUS: • Bawang merah 10 butir • Bawang putih 5 siung • Cabai merah besar 3 buah • Cabai rawit 100 gram",
        desc2 : "1.) Kerat-kerat bakso, kemudian goreng sebentar dalam minyak panas. Angkat dan tiriskan.                                  2.) Haluskan semua bumbu halus: Bawang merah, bawang putih, cabai merah dan cabai rawit. Kemudian, tumis bumbu bersama daun salam dan daun jeruk.                                                                               3.) Masukkan bakso dan air, aduk rata. Tambahkan garam dan gula sesuai selera.                                           4.) Masak sampai air menyusut dan bumbu meresap. Koreksi rasanya, jika sudah pas matikan api. Siap disajikan.",
    },
]

export {Data1};