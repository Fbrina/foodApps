import React from 'react'
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import 'react-native-gesture-handler';
import Screen from './Screen';
import Login from './Login';
import Register from './Register';
import Home from './Home';
import HomeScreen from './Home';
import AboutScreen from './AboutScreen';
import Desc from './Desc';
import { AntDesign } from '@expo/vector-icons';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

export default function Food() {
    return (
       <NavigationContainer>
        <Stack.Navigator initialRouteName="Screen" >
          <Stack.Screen name='Screen' component={Screen} options={{ headerShown: false }} />
          <Stack.Screen name='Login' component={Login} options={{ headerShown: false }} />
          <Stack.Screen name='Register' component={Register} options={{ headerShown: false }} />
          <Stack.Screen name='Home' component={Home} options={{ headerShown: false }} />
          <Stack.Screen name='Desc' component={Desc} options={{ headerShown: false }} />
          <Stack.Screen name='MyTabs' component={MyTabs} options={{ headerShown: false }} />
        </Stack.Navigator>
      </NavigationContainer>
    )
  function MyTabs() {
  return (
    <Tab.Navigator
    screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            let iconName;

            if (route.name === 'Home') {
              iconName = focused
                ? 'ios-information-circle'
                : 'ios-information-circle-outline';
                return <AntDesign name="home" size={24} color="black" />;
            } else if (route.name === 'About') {
              iconName = focused ? 'ios-list-box' : 'ios-list';
              return <AntDesign name="user" size={24} color="black" />
            }
          },
          tabBarActiveTintColor: 'tomato',
          tabBarInactiveTintColor: 'gray',
        })}
    >
      <Tab.Screen name="Home" component={Home} options={{ headerShown: false }}/>
      <Tab.Screen name="About" component={AboutScreen} options={{ headerShown: false }}/>
    </Tab.Navigator>
  );
}
}