import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View, Image, Button, SafeAreaView, ScrollView, TouchableOpacity } from 'react-native'
import { MaterialIcons } from '@expo/vector-icons';

export default function Desc({navigation, route}) {
  const item = route.params;
  return (
    <SafeAreaView style={{flex:1, backgroundColor: 'white'}}>
      <View style={styles.header}>
        <MaterialIcons name="arrow-back" size={28} color="black" onPress={()=>navigation.goBack()}/>
        <Text style={{alignSelf:'center',fontWeight:'bold', fontSize:24}}>{item.title}</Text>
        <MaterialIcons name="favorite" size={24} color="black" />
      </View>
      <View style={styles.imageContainer}>
        <Image style={{resizeMode:'contain', flex:1}} source={item.image} />
      </View>
      <View style={styles.detailsContainer}>
        <ScrollView>
          <Text style={{alignSelf:'center', fontWeight:'bold', fontSize:20}}>{item.desc}</Text>
          <Text style={{alignSelf:'flex-start', paddingLeft:20, paddingTop:10, fontSize:15}}>Bahan:</Text>
          <Text style={{alignSelf:'center',paddingHorizontal:35, fontSize:12}}>{item.desc1}</Text>
          <Text style={{alignSelf:'flex-start', paddingLeft:20, paddingTop:10, fontSize:15}}>Cara Membuat:</Text>
          <Text style={{alignSelf:'center',paddingHorizontal:35, fontSize:12}}>{item.desc2}</Text>
        </ScrollView>
      </View>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  header:{
    paddingHorizontal:20,
    marginTop:30,
    flexDirection:'row',
    justifyContent:'space-between'
  },
  imageContainer:{
    flex:0.45,
    marginTop:20,
    justifyContent:'center',
    alignItems:'center'
  },
  detailsContainer:{
    flex:0.55,
    backgroundColor:'#B9FFF2',
    marginHorizontal:7,
    borderRadius:20,
    marginTop:30,
    paddingTop:20
  }
})